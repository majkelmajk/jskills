import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './main.routes';

export class MainController {
  /*@ngInject*/
  constructor($location) {
    this.location = $location;
  }
  getSchemeAndHttpHost() {
    return this.location.absUrl();
  }
}


export default angular.module('jskillsApp.main', [uiRouter])
  .config(routing)
  .component('main', {
    template: require('./main.html'),
    controller: MainController
  })
  .name;
