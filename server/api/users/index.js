'use strict';

var express = require('express');
var controller = require('./users.controller');

var router = express.Router();

router.get('/', controller.list);
router.get('/:id', controller.index);

module.exports = router;
